//
//  ViewController.swift
//  MapJun
//
//  Created by Guzlat on 18/1/22.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController {

    let mapView: MKMapView = {
        let view = MKMapView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let addAdressBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Add adress", for: .normal)
        btn.backgroundColor = .blue
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()

    let routeBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Route", for: .normal)
        btn.backgroundColor = .blue
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isHidden = true
        return btn
    }()

    let resetBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Reset", for: .normal)
        btn.backgroundColor = .blue
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isHidden = true
        return btn
    }()

    var annotaionArray = [MKPointAnnotation]()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        mapView.delegate = self
        setConstraints()
        addAdressBtn.addTarget(self, action: #selector(addAdress), for: .touchUpInside)
        routeBtn.addTarget(self, action: #selector(addRoute), for: .touchUpInside)
        resetBtn.addTarget(self, action: #selector(reset), for: .touchUpInside)
        
    }
    @objc func addAdress(){
        print("add adress")
        alertAddAddres(title: "Add adress", placeholder: "Enter an adress") { text in
            self.setupPlacemark(adressPlace: text)
        }
    }

    @objc func addRoute(){
        print("route mapJun")
        
        for index in 0...annotaionArray.count - 2 {
            createDirectionRequest(startCoordinate: annotaionArray[index].coordinate,
                                   destinationCoordinate: annotaionArray[index+1].coordinate)
        }
        mapView.showAnnotations(annotaionArray, animated: true)
    }

    @objc func reset(){
        print("reset")
        mapView.removeOverlays(mapView.overlays)
        mapView.removeAnnotations(mapView.annotations)
        annotaionArray = [MKPointAnnotation]()
        routeBtn.isHidden = true
        resetBtn.isHidden = true
    }
    
    private func setupPlacemark(adressPlace: String){
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(adressPlace) {[self] placemarks, error in
            if let error = error{
                print(error)
                alertError(title: "Error", message: "Server is not avialable")
                return
            }
            
            guard let placemarks = placemarks else { return }
            let placemark = placemarks.first
            
            let annotation = MKPointAnnotation()
            annotation.title = "\(adressPlace)"
            
            guard let placeMarkLocation = placemark?.location else { return }
            annotation.coordinate = placeMarkLocation.coordinate
            
            annotaionArray.append(annotation)
            
            if annotaionArray.count > 2 {
                routeBtn.isHidden = false
                resetBtn.isHidden = false
            }
            
            mapView.showAnnotations(annotaionArray, animated: true)
            
        }
    }
    
    private func createDirectionRequest(startCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D){
        let startLocation = MKPlacemark(coordinate: startCoordinate)
        let destinationLocation = MKPlacemark(coordinate: destinationCoordinate)
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: startLocation)
        request.destination = MKMapItem(placemark: destinationLocation)
        request.transportType = .any
        request.requestsAlternateRoutes = true
        
        let direction = MKDirections(request: request)
        direction.calculate { (response, error) in
            if let error = error {
                print("error createDirection: \(error)")
                return
            }
            
            guard let response = response else {
                self.alertError(title: "Error", message: "Route is not avialable")
                return
            }
            
            var minRoute = response.routes[0]
            for route in response.routes {
                minRoute = route.distance < minRoute.distance ? route : minRoute
            }
            
            self.mapView.addOverlay(minRoute.polyline)
        }
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        renderer.strokeColor = .red
        return renderer
    }
}

extension  ViewController{
    func setConstraints() {
        view.addSubview(mapView)
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0)
        ])
        mapView.addSubview(addAdressBtn)
        NSLayoutConstraint.activate([
            addAdressBtn.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 50),
            addAdressBtn.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -24),
            addAdressBtn.heightAnchor.constraint(equalToConstant: 70),
            addAdressBtn.widthAnchor.constraint(equalToConstant: 100)
        ])
        
        mapView.addSubview(resetBtn)
        NSLayoutConstraint.activate([
            resetBtn.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -50),
            resetBtn.leadingAnchor.constraint(equalTo: mapView.leadingAnchor, constant: 24),
            resetBtn.heightAnchor.constraint(equalToConstant: 70),
            resetBtn.widthAnchor.constraint(equalToConstant: 100)
        ])
        
        mapView.addSubview(routeBtn)
        NSLayoutConstraint.activate([
            routeBtn.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -50),
            routeBtn.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -24),
            routeBtn.heightAnchor.constraint(equalToConstant: 70),
            routeBtn.widthAnchor.constraint(equalToConstant: 100)
        ]) 
    }
}

