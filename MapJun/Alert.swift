//
//  Alert.swift
//  MapJun
//
//  Created by Guzlat on 6/2/22.
//

import UIKit

extension UIViewController {
    func alertAddAddres(title: String, placeholder: String,
                        completionHandler: @escaping(String) -> Void){
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let alertOk = UIAlertAction(title: "OK", style: .default) { action in
            let tfText = alert.textFields?.first
            guard let text = tfText?.text else { return }
            completionHandler(text)
        }
        
        alert.addTextField { tf in
            tf.placeholder = placeholder
        }
        let alertCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(alertOk)
        alert.addAction(alertCancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func alertError(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertOK = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(alertOK)
        
        present(alertController, animated: true, completion: nil)
    }
}
